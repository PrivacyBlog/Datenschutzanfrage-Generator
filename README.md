# Datenschutzanfrage-Generator

Dieses Tool ist dafür gedacht, Datenschutzanfragen zu vereinfachen. Für zuvor mit [Webbkoll](https://webbkoll.dataskydd.net/) analysierte Webseiten können hiermit schnell und einfach Anfragen generiert werden, die dann z.B. per E-Mail versendet werden können.

Eine Live-Version ist hier zu finden:

https://www.privacy-blog.org/dsgenerator
